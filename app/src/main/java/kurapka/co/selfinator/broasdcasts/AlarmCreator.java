package kurapka.co.selfinator.broasdcasts;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by achmudas on 26/07/15.
 */
public class AlarmCreator {

    private static final String LOG_TAG = "AGE_TRACK";

    public static void createNotificationAlarm(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmReceiverIntent = new Intent("kurapka.co.agetracker.broadcasts.START_ALARM");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 2, alarmReceiverIntent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 19);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Log.i(LOG_TAG, "Setting alarm");
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, alarmIntent);
    }
}
