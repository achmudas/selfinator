package kurapka.co.selfinator.broasdcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by achmudas on 04/07/15.
 */
public class PhoneBootBroadcastReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "AGE_TRACK";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {

           AlarmCreator.createNotificationAlarm(context);
        }
    }
}
