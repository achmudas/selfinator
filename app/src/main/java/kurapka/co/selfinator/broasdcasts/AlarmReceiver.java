package kurapka.co.selfinator.broasdcasts;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import kurapka.co.selfinator.R;
import kurapka.co.selfinator.activities.HomeActivity;

/**
 * Created by achmudas on 04/07/15.
 */
public class AlarmReceiver extends BroadcastReceiver {

    private static final String LOG_TAG = "AGE_TRACK";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("kurapka.co.agetracker.broadcasts.START_ALARM")) {
            Log.i(LOG_TAG, "Issuing notification");
            Intent resultIntent = new Intent(context, HomeActivity.class);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(
                    context,
                    0,
                    resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );

            NotificationCompat.Builder selfieNot = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle("Age Tracker")
                    .setContentText("It's time for selfie!")
                    .setContentIntent(resultPendingIntent)
                    .setAutoCancel(true);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(15, selfieNot.build());
        }
    }
}
