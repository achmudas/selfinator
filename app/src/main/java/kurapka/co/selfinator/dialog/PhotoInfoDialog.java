package kurapka.co.selfinator.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import java.io.File;
import java.util.Date;

import kurapka.co.selfinator.R;

/**
 * Created by achmudas on 02/07/15.
 */
public class PhotoInfoDialog extends DialogFragment{

    private String photoPath;

    private static final String LOG_TAG = "AGE_TRACK";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().getString("photoPath") != null) {
            photoPath = getArguments().getString("photoPath");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstance) {
        Log.i(LOG_TAG, "Opening dialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_info)
                .setMessage("Date taken: " + new Date(new File(photoPath).lastModified()))
                .setNeutralButton(R.string.dialog_close, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }
}
