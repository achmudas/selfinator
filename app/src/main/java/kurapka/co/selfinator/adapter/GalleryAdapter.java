package kurapka.co.selfinator.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kurapka.co.selfinator.utilities.BitmapUtilities;

/**
 * Created by achmudas on 14/06/15.
 */
public class GalleryAdapter extends BaseAdapter {

    private static final String LOG_TAG = "AGE_TRACK";

    private final Context mContext;
    private List<String> imagePaths = new ArrayList<>();

    public GalleryAdapter(Context context, List<String> imagePaths) {
        this.mContext = context;
        this.imagePaths = imagePaths;
    }


    public void addImagePath(String imagePath) {
        imagePaths.add(imagePath);
    }

    public void removeImagePath(String imagePath) {
        imagePaths.remove(imagePath);
    }

    @Override
    public int getCount() {
        return imagePaths.size();
    }

    @Override
    public Object getItem(int position) {
        return imagePaths.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {

            Resources resources = Resources.getSystem();
            float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, resources.getDisplayMetrics());

            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams((int)pixels, (int)pixels));
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }
        String currentImagePath = imagePaths.get(position);

        int orientation = BitmapUtilities.getCurrentOrientation(currentImagePath);
        Bitmap bitmapImage = BitmapUtilities.rotateBitmap(BitmapUtilities.decodeBitmapFromUri(currentImagePath), orientation);

        imageView.setImageBitmap(bitmapImage);
        return imageView;
    }

}
