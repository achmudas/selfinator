package kurapka.co.selfinator.activities;

import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kurapka.co.selfinator.R;
import kurapka.co.selfinator.adapter.GalleryAdapter;
import kurapka.co.selfinator.broasdcasts.AlarmCreator;
import kurapka.co.selfinator.dialog.PhotoInfoDialog;


public class HomeActivity extends AppCompatActivity {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String LOG_TAG = "AGE_TRACK";

    private GalleryAdapter mAdapter;

    // TODO think something smarter
    private File imageFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        GridView mGalleryView = (GridView) findViewById(R.id.gallery_view);
        mAdapter = new GalleryAdapter(this, getImageUrls());
        mGalleryView.setAdapter(mAdapter);

        registerForContextMenu(mGalleryView);

        mGalleryView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent displayPhoto = new Intent(HomeActivity.this, PhotoViewActivity.class);
                displayPhoto.putExtra("imagePath", (String) mAdapter.getItem(position));
                startActivity(displayPhoto);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setLogo(R.mipmap.ic_launcher);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        boolean alarmUp = (PendingIntent.getBroadcast(getApplicationContext(), 2,
                new Intent("kurapka.co.agetracker.broadcasts.START_ALARM"),
                PendingIntent.FLAG_NO_CREATE) != null);

        if (!alarmUp) {
            Log.i(LOG_TAG, "Alarm is not launched, starting for the first time");
            AlarmCreator.createNotificationAlarm(this);
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo contextMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case R.id.info:
                Log.i(LOG_TAG, "Selected id: " + contextMenuInfo.position);
                showInfo(mAdapter.getItem(contextMenuInfo.position));
                return true;
            case R.id.delete:
                Log.i(LOG_TAG, "Selected id: " + contextMenuInfo.position);
                deletePhoto(mAdapter.getItem(contextMenuInfo.position));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deletePhoto(Object photoPath) {
        Log.i(LOG_TAG, "Deleting photo: " + photoPath);
        mAdapter.removeImagePath((String) photoPath);
        File fileToDelete = new File((String) photoPath);
        boolean isDeleted = fileToDelete.delete();
        if (isDeleted) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void showInfo(Object photoPath) {
        Log.i(LOG_TAG, "Showing info about photo: " + photoPath);
        Bundle bundle = new Bundle();
        bundle.putString("photoPath", (String) photoPath);
        DialogFragment infoDialog = new PhotoInfoDialog();
        infoDialog.setArguments(bundle);
        infoDialog.show(getFragmentManager(), null);
    }

    private List<String> getImageUrls() {
        Log.i(LOG_TAG, "Starting to load images for grid");
        List<String> ageTrackImages = new ArrayList<>();
        File externalStorage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File [] allImages = externalStorage.listFiles();
        for (File image : allImages) {
            if (image.getName().contains("age_")) {
                Log.i(LOG_TAG, image.getName());
                ageTrackImages.add(image.getAbsolutePath());
            }
        }
        Log.i(LOG_TAG, "Finished to load images for grid");
        return ageTrackImages;
    }


    @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
       if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

           if (imageFile != null) {
               mAdapter.addImagePath(imageFile.getPath());
               mAdapter.notifyDataSetChanged();
           }
       }
   }

   private File createTempPhoto() throws IOException {
       String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
       String imageName = "age_" + timeStamp;
       File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
       File image = File.createTempFile(
               imageName,
               ".png",
               storageDir
       );
       return image;
   }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeActivity/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.camera) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

                try {
                    imageFile = createTempPhoto();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (imageFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile));
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
