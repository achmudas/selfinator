package kurapka.co.selfinator.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;

import kurapka.co.selfinator.R;
import kurapka.co.selfinator.utilities.BitmapUtilities;

/**
 * Created by achmudas on 26/06/15.
 */
public class PhotoViewActivity extends Activity{

    private static final String LOG_TAG = "AGE_TRACK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ImageView imageView = (ImageView) findViewById(R.id.photo_view);

        String imagePath = getIntent().getExtras().getString("imagePath");
        Log.d(LOG_TAG, "Image path: " + imagePath);

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
        } catch (IOException ex) {
            Log.e(LOG_TAG, ex.getMessage());
            ex.printStackTrace();
        }

        int orientation = exif != null ?
                exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
                : ExifInterface.ORIENTATION_NORMAL;

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        Bitmap rotatedBitmapImage = BitmapUtilities.rotateBitmap(bitmap, orientation);
        imageView.setImageBitmap(rotatedBitmapImage);

    }



}
