package kurapka.co.selfinator.activities;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.ImageView;

import kurapka.co.selfinator.R;

/**
 * Created by achmudas on 10/07/15.
 */
public class PhotoViewActivityTest extends ActivityInstrumentationTestCase2<PhotoViewActivity> {

    private PhotoViewActivity activity;
    private ImageView imageView;

    public PhotoViewActivityTest() {
        super(PhotoViewActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        Intent mockIntent = new Intent();
        mockIntent.putExtra("imagePath", "fakeimagepath/photo.png");
        setActivityIntent(mockIntent);
        activity = getActivity();
        imageView = (ImageView) activity.findViewById(R.id.photo_view);
    }

    public void testThatImageViewIsNotEmpty() {
        assertNotNull(imageView);
        assertEquals(null, imageView.getDrawable());
    }
}
